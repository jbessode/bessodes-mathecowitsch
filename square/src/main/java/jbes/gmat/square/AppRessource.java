package jbes.gmat.square;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This class groups the different endpoints of the server allowing to deploy, stop an application and list the working applications.
 * <ul>
 * <li>appManager is the parameter allowing to manage the different applications.</li>
 * </ul>
 * @author J.Bessodes G.Mathecowitsch 
 */
@Path("/app")
@Produces(MediaType.APPLICATION_JSON)
public class AppRessource {
	
	/**
	 * appManager is the parameter allowing to manage the different applications.
	 */
	private final ApplicationManager appManager = new ApplicationManager();
	
	
	/**
	 * This method allows to deploy an application from creating it to running it, and sending back a response.
	 * 
	 * @param js
	 * 		JS is the JSON Object of the application you want to deploy.
	 * @return
	 * 		This method return a reply sending containing a JSON.
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/deploy")
    public Response deploy(JsonObject js) {
		try {
			var appString = js.get("app").toString();
			var dataString = appString.substring(1, appString.length()-1).split(":", 2);
			Application app = Application.create(appManager.getNextId(), dataString[0], Integer.parseInt(dataString[1]), appManager.getNextServicePort(), appManager.getDockerInstanceName(dataString[0]));
			appManager.add(app);
			return Response.ok(app.toJSON(false)).build();
		} catch (Exception e) {
			return Response.serverError().entity("erreur : " + e.getMessage() + ", nature : " + e.getClass()).build();
		}
    }
	
	/**
	 * This method allows to send deploy a list of running application.
	 * 
	 * @return
	 * 		This method return a reply sending containing a JSON.
	 */
	@GET
	@Path("/list")
    public Response list() {
		try {
			appManager.checkContainer();
			return Response.ok(appManager.getList()).build();
		} catch (Exception e) {
			return Response.serverError().entity("erreur : " + e.getMessage() + ", nature : " + e.getClass()).build();
		}
    }
	
	/**
	 * This method allows to stop an application and sending back a response.
	 * 
	 * @param js
	 * 		JS is the JSON Object of the application you want to stop.
	 * @return
	 * 		This method return a reply sending containing a JSON.
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/stop")
    public Response stop(JsonObject js) {
			try {
				if (js.containsKey("id") && js.size() == 1) {
					Application app = appManager.getAppById(js.getInt("id"));
					SquareProcess.stop(app.getDockerInstance());
					appManager.remove(app);
					return Response.ok(app.toJSON(true)).build();
				}
				else return Response.serverError().entity("JSON invalid format").build();
			} catch (Exception e) {
				return Response.serverError().entity("erreur : " + e.getMessage() + ", nature : " + e.getClass()).build();
			}	
    }
}