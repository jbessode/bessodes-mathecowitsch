package jbes.gmat.square;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.json.Json;
import javax.json.JsonObject;

/**
 * This class allows to create applications usable by docker .
 * <ul>
 * <li>id is a parameter giving an information of the id of the application.</li>
 * <li>appName is the name of the application.</li>
 * <li>port is the port used by the application.</li>
 * <li>dockerInstance is the name of the docker instance of the application.</li>
 * <li>servicePort is the service port used by the application.</li>
 * <li>time is the time at which the application was created</li>
 * </ul>
 * @author  G.Mathecowitsch J.Bessodes 
 */
public class Application {
	/**
	 * id is a parameter giving an information of the id of the application.
	 */
	private final long id;
	
	/**
	 * appName is the name of the application.
	 */
	private final String appName;
	
	/**
	 * port is the port used by the application.
	 */
	private final long port;
	
	/**
	 * dockerInstance is the name of the docker instance of the application.
	 */
	private final String dockerInstance;
	
	/**
	 * servicePort is the service port used by the application.
	 */
	private final long servicePort;
	
	/**
	 * time is the time at which the application was created.
	 */
	private final long timeBegin;
	
	
	/**
	 *  <b>Constructor of Application</b>
	 */
	private Application(long id, String appName, long port, String dockerInstance, long servicePort, long timeBegin) {
		this.id = id;
		this.appName = appName;
		this.port = port;
		this.dockerInstance = dockerInstance;
		this.servicePort = servicePort;
		this.timeBegin = timeBegin;
	}

	/**
	 * This method allows to create an object Application and create it in docker.
	 * 
	 * @param id
	 * 		id of the app to create
	 * @param appName
	 * 		name of the app to create
	 * @param port
	 * 		port to use for the app to create
	 * @param servicePort
	 * 		service port to use for the app to create
	 * @param dockerInstance
	 * 		name of the docker Instance of the app to create
	 * @return
	 * 		This method returns the created application.
	 */
	public static Application create(long id, String appName, long port, long servicePort, String dockerInstance) {
		if (!applicationExists(appName)) throw new IllegalStateException("jar doesn't exists !");
		var date = new Date();
		var app = new Application(id, appName, port, dockerInstance, servicePort, date.getTime());
		SquareProcess.deploy(app);	
		return app;
	}
	
	/**
	 * This method allows to check if a application already exists.
	 * 
	 * @param appName
	 * 		name of the application to check
	 * @return
	 * 		this method returns true if the app exists or flse if not.
	 */
	private static boolean applicationExists(String appName) {
		String pathFile = System.getProperty("user.dir") + "/../../apps/" + appName + ".jar";
		File file = new File(pathFile);
		return file.exists();
	}

	/**
	 * This method give the name of an application.
	 * 
	 * @return
	 * 		This method return the name of an application.
	 */
	public String getApplicationName() {
		return appName;
	}
	
	/**
	 * This method allows to create a JSON Object from an application with the elapsed time from it's creation or not.
	 * 
	 * @param withTime
	 * 		this parameter is true if the time need to be added or false if it is not needed.
	 * @return
	 * 		this method returns the created JSON Object from the application.
	 */
	public JsonObject toJSON(boolean withTime) {
		var jsonToBuild = Json.createObjectBuilder()
			     .add("id", id)
			     .add("app", appName)
			     .add("port", port)
			     .add("service-port", servicePort)
			     .add("docker-instance", dockerInstance);
		if (withTime) {
			jsonToBuild.add("elapsed-time", calculTime());
		}
		return jsonToBuild.build();
	}
	
	/**
	 * This method allows to calculate the elapsed time of an application from it's creation.
	 * 
	 * @return
	 * 		this method
	 */
	private String calculTime() {
		var format = new SimpleDateFormat("mm'm'ss's'");
		var actualTime = new Date().getTime();
		String dateString = format.format(new Date(actualTime - timeBegin));
		return dateString;
	}
	
	/**
	 * This method give the port of an application
	 * 
	 * @return
	 * 		It returns the port of an application
	 */
	public long getPort() {
		return port;
	}

	/**
	 * This method give the docker Instance of an application
	 * 
	 * @return
	 * 		It returns the docker instance of an application
	 */
	public String getDockerInstance() {
		return dockerInstance;
	}

	/**
	 * This method give the id of an application
	 * 
	 * @return
	 * 		It returns the id of an application
	 */
	public long getId() {
		return id;
	}

	/**
	 * This method give the service port of an application
	 * 
	 * @return
	 * 		It returns the service port of an application
	 */
	public long getServicePort() {
		return servicePort;
	}

	/**
	 * This method allows to display the informations of an application.
	 */
	@Override
	public String toString() {
		return "Application(" + id + "-" + dockerInstance + ")";
	}

	/**
	 * This method allows to compare two applications.
	 * 
	 * @return
	 * 		This method return true if two applications are the same or false if not.
	 */
	@Override
	public boolean equals(Object obj) {
		Application other = (Application) obj;
		if (id != other.id)	return false;
		return true;
	}
}
